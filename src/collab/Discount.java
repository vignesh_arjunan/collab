/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collab;

/**
 *
 * @author vignesh
 */
public class Discount {

    private final int quantity;
    private final double value;

    public Discount(int quantity, double value) {
        this.quantity = quantity;
        this.value = value;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getValue() {
        return value;
    }
}
