/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collab;

import java.util.Optional;

/**
 *
 * @author vignesh
 */
public interface Pricing {

    double getPrice(String item, int quantity, Optional<Discount> discount);
}
