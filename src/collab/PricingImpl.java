/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collab;

import collab.handlers.ApplePriceHandler;
import collab.handlers.OrangePriceHandler;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author vignesh
 */
public class PricingImpl implements Pricing {

    Map<String, PricingHandler> handlers = new HashMap<>();

    public PricingImpl() {
        handlers.put("apple", new ApplePriceHandler());
        handlers.put("orange", new OrangePriceHandler());
    }

    @Override
    public double getPrice(String item, int quantity, Optional<Discount> discount) {
        if (handlers.containsKey(item)) {
            return handlers.get(item).getPrice(quantity, discount);
        }
        throw new IllegalArgumentException(item + " not found");
    }

}
