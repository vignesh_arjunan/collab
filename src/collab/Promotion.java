/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collab;

/**
 *
 * @author vignesh
 */
public class Promotion {

    private final String item;
    private final Discount discount;

    public Promotion(String item, Discount discount) {
        this.item = item;
        this.discount = discount;
    }

    public String getItem() {
        return item;
    }

    public Discount getDiscount() {
        return discount;
    }
}
