/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collab.handlers;

import collab.Discount;
import collab.PricingHandler;
import java.util.Optional;

/**
 *
 * @author vignesh
 */
public class ApplePriceHandler implements PricingHandler {

    @Override
    public double getPrice(int quantity, Optional<Discount> optDiscount) {
        return PriceCalculator.getPrice(50.0, quantity, optDiscount);
    }

}
