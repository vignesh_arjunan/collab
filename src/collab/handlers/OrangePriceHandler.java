/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collab.handlers;

import collab.Discount;
import collab.PricingHandler;
import java.util.Optional;

/**
 *
 * @author vignesh
 */
public class OrangePriceHandler implements PricingHandler {

    @Override
    public double getPrice(int quantity, Optional<Discount> opDiscount) {
        return PriceCalculator.getPrice(30.0, quantity, opDiscount);
    }

}
