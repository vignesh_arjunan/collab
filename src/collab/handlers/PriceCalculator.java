/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collab.handlers;

import collab.Discount;
import java.util.Optional;

/**
 *
 * @author vignesh
 */
public class PriceCalculator {

    public static double getPrice(double unitPrice, int quantity, Optional<Discount> optDiscount) {
        if (!optDiscount.isPresent()) {
            return unitPrice * quantity;
        }
        Discount discount = optDiscount.get();
        return (((quantity % discount.getQuantity()) * unitPrice)
                + ((quantity / discount.getQuantity()) * discount.getValue()));
    }
}
