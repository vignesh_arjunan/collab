/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collab;

import java.util.Optional;
import java.util.ServiceLoader;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author vignesh
 */
public class TestPricing {

    public TestPricing() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void anAppleCost() {
        Pricing pricing = ServiceLoader.load(Pricing.class).iterator().next();
        Assert.assertEquals(50, pricing.getPrice("apple", 1, Optional.ofNullable(null)), 0);
    }

    @Test
    public void twoApplesCost() {
        Pricing pricing = ServiceLoader.load(Pricing.class).iterator().next();
        Assert.assertEquals(100, pricing.getPrice("apple", 2, Optional.ofNullable(null)), 0);
    }

    @Test
    public void fourApplesCost() {
        Pricing pricing = ServiceLoader.load(Pricing.class).iterator().next();
        Assert.assertEquals(200, pricing.getPrice("apple", 4, Optional.ofNullable(null)), 0);
    }

    @Test
    public void fourApplesCostWithDiscount() {
        Pricing pricing = ServiceLoader.load(Pricing.class).iterator().next();
        Assert.assertEquals(180, pricing.getPrice("apple", 4, Optional.ofNullable(new Discount(3, 130))), 0);
    }

    @Test
    public void anOrangeCost() {
        Pricing pricing = ServiceLoader.load(Pricing.class).iterator().next();
        Assert.assertEquals(30, pricing.getPrice("orange", 1, Optional.ofNullable(null)), 0);
    }

    @Test
    public void twoOrangesCost() {
        Pricing pricing = ServiceLoader.load(Pricing.class).iterator().next();
        Assert.assertEquals(45, pricing.getPrice("orange", 2, Optional.ofNullable(new Discount(2, 45))), 0);
    }

    @Test
    public void fourOrangesCostWithDiscount() {
        Pricing pricing = ServiceLoader.load(Pricing.class).iterator().next();
        Assert.assertEquals(90, pricing.getPrice("orange", 4, Optional.ofNullable(new Discount(2, 45))), 0);
    }

    @Test
    public void fiveOrangesCostWithDiscount() {
        Pricing pricing = ServiceLoader.load(Pricing.class).iterator().next();
        Assert.assertEquals(120, pricing.getPrice("orange", 5, Optional.ofNullable(new Discount(2, 45))), 0);
    }

    @Test
    public void fiveOrangesCost() {
        Pricing pricing = ServiceLoader.load(Pricing.class).iterator().next();
        Assert.assertEquals(150, pricing.getPrice("orange", 5, Optional.ofNullable(null)), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void twoWhatEverCost() {
        Pricing pricing = ServiceLoader.load(Pricing.class).iterator().next();
        Assert.assertEquals(60, pricing.getPrice("whatever", 2, Optional.ofNullable(null)), 0);
    }
}
